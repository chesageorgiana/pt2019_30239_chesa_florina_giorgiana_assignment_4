package restaurant;

import model.MenuItem;
import model.Order;

import java.io.Serializable;
import java.util.*;

public class Restaurant implements RestaurantProc, Serializable {

    private List<MenuItem> allRestaurantItems;
    private Map<Order, List<MenuItem>> restaurantHashMap;

    public Restaurant() {
        restaurantHashMap = new HashMap<>();
        allRestaurantItems = new ArrayList<>();
    }

    public boolean isWellFormed() {
        int countAllList = 0;
        List<MenuItem> list = new ArrayList<>();
        Collection<List<MenuItem>> collection = restaurantHashMap.values();
        for (List<MenuItem> menuItems : collection) {
            list = menuItems;
            countAllList++;
            if (list.isEmpty())
                return false;
            else {
                for (MenuItem menuItem : list)
                    if (!(menuItem instanceof MenuItem))
                        return false;
            }
        }
        if (countAllList == restaurantHashMap.size())
            return true;
        if (countAllList != restaurantHashMap.size())
            return false;
        return true;
    }

    @Override
    public void addNewMenuItem(MenuItem menuItem) {
        this.allRestaurantItems.add(menuItem);
    }

    @Override
    public void deleteMenuItem(MenuItem menuItem) {
        this.allRestaurantItems.remove(menuItem);
    }

    @Override
    public void updateMenuItem(MenuItem menuItem) {
        this.allRestaurantItems.forEach(p -> {
            if (p.getId().equals(menuItem.getId())) {
                this.allRestaurantItems.remove(p);
            }
        });
        this.allRestaurantItems.add(menuItem);
    }

    @Override
    public void addOrder(Order order, List<MenuItem> menuItemList) {
        this.restaurantHashMap.put(order, menuItemList);
    }

    @Override
    public void deleteOrder(Order order) {
        this.restaurantHashMap.remove(order);
    }


    @Override
    public void editOrder() {

    }

    public List<MenuItem> getAllRestaurantItems() {
        return allRestaurantItems;
    }

    public void setAllRestaurantItems(List<MenuItem> allRestaurantItems) {
        this.allRestaurantItems = allRestaurantItems;
    }

    public Map<Order, List<MenuItem>> getRestaurantHashMap() {
        return restaurantHashMap;
    }

    public void setRestaurantHashMap(Map<Order, List<MenuItem>> restaurantHashMap) {
        this.restaurantHashMap = restaurantHashMap;
    }
}
