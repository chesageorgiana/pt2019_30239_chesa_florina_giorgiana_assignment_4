package model;

public class VeggieItem extends MenuItem {

    public VeggieItem(String name, Double price) {
        super(name, price);
        this.setType("Vegetal/Vegan");
    }
}
