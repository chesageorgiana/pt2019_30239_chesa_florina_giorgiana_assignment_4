package serializable;

import model.MenuItem;
import model.Order;
import restaurant.Restaurant;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SerializableOperations {

    public static void writeRestaurantMapToFile(Restaurant restaurant) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream("src/main/resources/restaurant.bin");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(restaurant.getRestaurantHashMap());
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (Exception e) {
            System.err.println("Serialization error!");
            e.printStackTrace();
        }
    }

    public static Map<Order,List<MenuItem>> readRestaurantFromFile() {
        Map<Order, List<MenuItem>> map = null;
        try {
            FileInputStream fileInputStream = new FileInputStream("src/main/resources/restaurant.bin");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            map = (HashMap) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
            return map;
        } catch (Exception e) {
            System.err.println("Deserialization error!");
            e.printStackTrace();
            return null;
        }
    }

    public static void writeItemsToFile(Restaurant restaurant) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream("src/main/resources/items.bin");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(restaurant.getAllRestaurantItems());
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (Exception e) {
            System.err.println("Serialization error!");
            e.printStackTrace();

        }
    }

    public static List<MenuItem> readItemsFromFile() {
        List<MenuItem> list = null;
        try {
            FileInputStream fileInputStream = new FileInputStream("src/main/resources/items.bin");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            list = (ArrayList) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
            return list;
        } catch (Exception e) {
            System.err.println("Deserialization error!");
            e.printStackTrace();
            return null;
        }
    }
}
